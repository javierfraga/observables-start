import { Component, OnInit,OnDestroy } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import {Subscription} from 'rxjs/Subscription';
import 'rxjs/Rx';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit,OnDestroy {
	numSubscription:Subscription;
	customSubscription:Subscription;

	constructor() { }

	ngOnInit() {
		const myNumbers = Observable.interval(1000)
		.map(
			(data:number)=>{
				return data*2;
			}
		);
		this.numSubscription = myNumbers.subscribe(
			(number:number)=>{
				console.log(number);
			}
		);

		// create observable
		const myObservable = Observable.create((Observer:Observer<string>)=>{
			setTimeout(()=>{
				Observer.next('first package');
			}, 2000);
			setTimeout(()=>{
				Observer.next('second package');
			}, 4000);
			setTimeout(()=>{
				Observer.error('this does not work');
			}, 5000);
			// setTimeout(()=>{
			// 	Observer.complete();
			// }, 5000);
			/*
			 * this one run after either .error() or .complete()
			 */
			setTimeout(()=>{
				Observer.next('third package');
			}, 6000);
		});
		// subscribe to Observable, looks like http request
		this.customSubscription = myObservable.subscribe(
			(next:string)=>{console.log(next);},
			(error:string)=>{console.log(error);},
			(complete:any)=>{console.log('completed');},
		);
	}
	ngOnDestroy() {
		this.numSubscription.unsubscribe();
		this.numSubscription.unsubscribe();
	}

}
