import { Component,OnInit } from '@angular/core';
import {UserService} from './user/users.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
	user1Activated = false;
	user2Activated = false;

	constructor(
		private UserService:UserService,
	) {}

	ngOnInit() {
		this.UserService.userActivated.subscribe(
			(next:number)=>{
				var id = next;
				if (id===1) { 
					this.user1Activated = true;
				} else if(id===2){
					this.user2Activated = true;	
				}
			}
		)
	}
}
